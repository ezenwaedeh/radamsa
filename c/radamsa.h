#include <inttypes.h>
#include <stddef.h>

extern void init();

extern size_t radamsa_inplace(uint8_t *ptr, size_t len, size_t max, unsigned int seed);

